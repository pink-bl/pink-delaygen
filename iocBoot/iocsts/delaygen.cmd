#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

#drvAsynIPPortConfigure("LDG1","172.17.10.58:5024",0,0,0)
drvAsynIPPortConfigure("LDG1","$(DEVIP):5024",0,0,0)

# Initialize input/output EOS
asynOctetSetOutputEos("LDG1",0,"\n")
asynOctetSetInputEos("LDG1",0,"\r\n")

## drvAsynDG645(myport,ioport,ioaddr)
#       myport  - Interface asyn port name (i.e. "DG0")
#       ioport  - Comm asyn port name (i.e. "L2")
#       ioaddr  - Comm asyn port addr
#
drvAsynDG645("DL1","LDG1",-1)

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
#dbLoadRecords("drvDG645.db","P=PINK:,R=DG01:,PORT=DL1")
dbLoadRecords("$(DELAYGEN)/db/drvDG645.db","P=PINK:,R=DG01:,PORT=DL1")

## Autosave settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## Create Autosave monitor
create_monitor_set("auto_settings.req", 30, "P=PINK:,R=DG01:")

## Output Amplitude
dbpf(PINK:DG01:ABOutputAmpAO, 2.5)

## Start any sequence programs
#seq sncxxx,"user=epics"
